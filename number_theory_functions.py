from random import randrange

def extended_gcd(a,b):
    """
    Returns the extended gcd of a and b

    Parameters
    ----------
    a : Input data.
    b : Input data.
    Returns
    -------
    (d, x, y): d = gcd(a,b) = a*x + b*y
    """
    if a == 0 :  
        return b, 0 , 1

    gcd, z, w = extended_gcd(b % a, a) 

    coefficientA = w - (b // a) * z
    coefficientB = z 
     
    return gcd, coefficientA, coefficientB
      

def NumberToBinaryString(a):
    return str(bin(a))[2:]


def modular_inverse(a,n):
    """
    Returns the inverse of a modulo n if one exists

    Parameters
    ----------
    a : Input data.
    n : Input data.

    Returns
    -------
    x: such that (a*x % n) == 1 and 0 <= x < n if one exists, else None
    """


def modular_exponent(a, d, n):
    """
    Returns a to the power of d modulo n

    Parameters
    ----------
    a : The exponential's base.
    d : The exponential's exponent.
    n : The exponential's modulus.

    Returns
    -------
    b: such that b == (a**d) % n
    """
    binB = NumberToBinaryString(d)
    ans = a % n
    l = len(binB)
    ret = 1

    for i, binLetter in enumerate(binB):
        power = l - i - 1
        pp = (2 ** power) % n
        x = (ans ** (int(binLetter) * pp)) % n
        ret = (ret * x) % n
    return ret

def miller_rabin(n):
    """
    Checks the primality of n using the Miller-Rabin test

    Parameters
    ----------
    n : The number to check

    Returns
    -------
    b: If n is prime, b is guaranteed to be True.
    If n is not a prime, b has a 3/4 chance at least to be False
    """
    a = randrange(1,n)
    k = 0
    d = n-1
    while d % 2 == 0:
        k = k + 1
        d = d // 2
    x = modular_exponent(a, d, n)
    if x == 1 or x == n-1:
        return True
    for _ in range(k):
        x = (x * x) % n
        if x == 1:
            return False
        if x == n-1:
            return True
    return False

def is_prime(n):
    """
    Checks the primality of n

    Parameters
    ----------
    n : The number to check

    Returns
    -------
    b: If n is prime, b is guaranteed to be True.
    If n is not a prime, b has a chance of less than 1e-10 to be True
    """
    for _ in range(10):
        if not miller_rabin(n):
            return False
    return True

def generate_prime(digits):
    for i in range(digits * 10):
        n = randrange(10**(digits-1), 10**digits)
        if is_prime(n):
            return n
    return None


def Question_1():
    print('#################### Q1 ##########################################')
    # 797*x+5279*y=gcd(797,5279)
    # 5279=6*797 + 497 // 5279-(),497==5279%797
    totalAmount = 1000000
    coinValue = 797
    billValue = 5279
    gcdResult, amountOfCoins, amountOfBills =  extended_gcd(coinValue, billValue)
    if totalAmount % gcdResult == 0 and amountOfBills < 0:
        print('It is posssible')
        print('Possible solution: ' + 'Spiderman pays ' + str(totalAmount * amountOfCoins) 
            + ' coins. '+ 'Luki returns ' + str(totalAmount * amountOfBills) + ' bills.')



def Question_2():
    print('#################### Q2 ##########################################')
    ans = modular_exponent(3, modular_exponent(2, 4, 400), 1000)
    finalAns = ans//100
    if(finalAns != 7):
        print(' test1 wrong ans:' + str(finalAns))

    ans = modular_exponent(4, modular_exponent(3, 2, 400), 1000)
    finalAns = ans//100
    if(finalAns != 1):
        print(' test2 wrong ans:' + str(finalAns))

    ans = modular_exponent(123456, modular_exponent(7896543, 74365753, 400), 1000)
    finalAns = ans//100
    print('hundreds digit of requested number is: ' + str(finalAns))



def Question_5():
    print('#################### Q5 ##########################################')
    p = 7919
    q = 6841
    k = (p-1)*(q-1)
    N = p * q

    Uk = []
    for i in range(k):
        gcdResult, x, y =  extended_gcd(k, i)
        if gcdResult == 1:
            Uk.append(i)
            if len(Uk) == 5:
                break
    
    e = Uk[1]
    gcdResult, x, y =  extended_gcd(e, k)
    d = x + k
    ourMessage = 4
    print('our message = ' + str(ourMessage))
    print('public key = ' + str(e))
    encrypted = modular_exponent(ourMessage, e, N)
    print('encrypted message = ' + str(encrypted))

    decrypted = modular_exponent(encrypted, d, N)
    if decrypted != ourMessage:
        print('error decrypted = ' + str(decrypted) + ' is different from original message = ' + str(ourMessage))

print()
Question_1()
print()
Question_2()
print()
Question_5()    
print()
